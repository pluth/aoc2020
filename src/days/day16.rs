use std::ops::RangeInclusive;

use aoc_runner_derive::*;
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug)]
struct Rule {
    range: RangeInclusive<i64>,
}

#[aoc(day16, part1)]
fn part1(input: &str) -> i64 {
    let mut all_rules = Vec::new();
    let mut parts = input.split("\n\n");
    let rules = parts.next().unwrap().lines();

    lazy_static! {
        static ref RULE_REGEX: Regex = Regex::new(r"^(.+): (\d+)\-(\d+) or (\d+)-(\d+)").unwrap();
    }

    for rule in rules {
        let captures = RULE_REGEX.captures(rule).unwrap();

        let rule1 = Rule {
            range: captures.get(2).unwrap().as_str().parse().unwrap()
                ..=captures.get(3).unwrap().as_str().parse().unwrap(),
        };

        let rule2 = Rule {
            range: captures.get(4).unwrap().as_str().parse().unwrap()
                ..=captures.get(5).unwrap().as_str().parse().unwrap(),
        };

        all_rules.push(rule1);
        all_rules.push(rule2);
    }

    let mut sum = 0;
    let other_tickets = parts.skip(1).next().unwrap().lines().skip(1);
    for ticket in other_tickets {
        let numbers = ticket.split(',');
        for number in numbers {
            let number: i64 = number.parse().unwrap();
            print!("{}: ", number);
            if !all_rules.iter().any(|rule| rule.range.contains(&number)) {
                sum += number;
            // println!("invalid");
            } else {
                // println!("valid");
            }
        }
    }

    sum
}

#[aoc(day16, part2)]
fn part2(input: &str) -> i64 {
    let mut all_rules = Vec::new();
    let mut parts = input.split("\n\n");
    let rules = parts.next().unwrap().lines();

    lazy_static! {
        static ref RULE_REGEX: Regex = Regex::new(r"^(.+): (\d+)\-(\d+) or (\d+)-(\d+)").unwrap();
    }

    for rule in rules {
        let captures = RULE_REGEX.captures(rule).unwrap();

        let rule1 = Rule {
            range: captures.get(2).unwrap().as_str().parse().unwrap()
                ..=captures.get(3).unwrap().as_str().parse().unwrap(),
        };

        let rule2 = Rule {
            range: captures.get(4).unwrap().as_str().parse().unwrap()
                ..=captures.get(5).unwrap().as_str().parse().unwrap(),
        };

        all_rules.push(rule1);
        all_rules.push(rule2);
    }

    let _my_ticket = parts.next().unwrap().lines().skip(1).next().unwrap();
    let _other_tickets = parts.next().unwrap().lines().skip(1);

    0
}
