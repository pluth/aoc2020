use aoc_runner_derive::*;
use std::collections::HashMap;

#[aoc(day14, part1)]
fn part1(input: &str) -> i64 {
    let mut memory = HashMap::new();
    let mut mask = "";

    for line in input.lines() {
        if let Some(new_mask) = line.strip_prefix("mask = ") {
            mask = new_mask;
        }

        if let Some(mem) = line.strip_prefix("mem[") {
            let mut split = mem.split("]");
            let index: i64 = split.next().unwrap().parse().unwrap();

            let mut value: i64 = split
                .next()
                .and_then(|s| s.strip_prefix(" = "))
                .and_then(|s| s.parse().ok())
                .unwrap();

            for (i, c) in mask.chars().rev().enumerate() {
                match c {
                    '1' => {
                        value |= 1 << i;
                    }
                    '0' => {
                        value &= !(1 << i);
                    }
                    _ => (),
                }
            }
            memory.insert(index, value);
        }
    }

    memory.values().sum()
}

#[aoc(day14, part2)]
fn part2(input: &str) -> i64 {
    let mut memory = HashMap::new();
    let mut mask = "";

    let mut variations = Vec::new();

    for line in input.lines() {
        if let Some(new_mask) = line.strip_prefix("mask = ") {
            mask = new_mask;
        }

        if let Some(mem) = line.strip_prefix("mem[") {
            let mut split = mem.split("]");
            let mut index: i64 = split.next().unwrap().parse().unwrap();

            let value: i64 = split
                .next()
                .and_then(|s| s.strip_prefix(" = "))
                .and_then(|s| s.parse().ok())
                .unwrap();

            for (i, c) in mask.chars().rev().enumerate() {
                match c {
                    '1' => {
                        index |= 1 << i;
                    }
                    '0' => {
                        // index &= !(1 << i);
                    }
                    _ => {
                        variations.push(i);
                    }
                }
            }
            apply(&variations, index, value, &mut memory);
            variations.clear();
        }
    }

    memory.values().sum()
}

fn apply(variations: &[usize], index: i64, value: i64, map: &mut HashMap<i64, i64>) {
    if let Some(offset) = variations.first() {
        apply(&variations[1..], index | 1 << offset, value, map);
        apply(&variations[1..], index & !(1 << offset), value, map);
    } else {
        map.insert(index, value);
    }
}
