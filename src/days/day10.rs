use std::{collections::BTreeMap, str::FromStr};

use aoc_runner_derive::*;

fn parse_numbers(input: &str) -> Result<Vec<i64>, <i64 as FromStr>::Err> {
    input.lines().map(str::parse).collect()
}

#[aoc_generator(day10, part2)]
fn generate(input: &str) -> anyhow::Result<Vec<i64>> {
    let mut numbers = parse_numbers(input)?;
    numbers.push(0);
    numbers.sort_unstable();
    numbers.push(*numbers.last().unwrap() + 3);

    Ok(numbers)
}

#[aoc(day10, part1)]
fn part1(input: &str) -> anyhow::Result<i64> {
    let mut numbers = parse_numbers(input)?;

    numbers.sort_unstable();

    let mut threes = 1;
    let mut ones = *numbers.first().unwrap();

    for [a, b] in numbers.array_windows() {
        match b - a {
            1 => ones += 1,
            3 => threes += 1,
            _ => (),
        }
    }

    Ok(threes * ones)
}

static COBINATIONS: &[u64] = &[0, 1, 2, 4, 7];

#[aoc(day10, part2)]
fn part2(numbers: &[i64]) -> anyhow::Result<u64> {
    let mut combinations: u64 = 1;

    let mut seq_len: u32 = 0;
    for [a, b] in numbers.array_windows() {
        match b - a {
            1 => {
                seq_len += 1;
            }
            3 => {
                if seq_len > 0 {
                    let c = COBINATIONS[seq_len as usize];
                    combinations *= c;
                }
                seq_len = 0;
            }
            i => panic!("unsupported gap: {}", i),
        }
    }

    Ok(combinations)
}

#[aoc(day10, part2, dp)]
fn part22(numbers: &[i64]) -> anyhow::Result<usize> {
    let mut memory = BTreeMap::new();
    let answer = paths_to_end(0, &numbers, &mut memory);
    Ok(answer)
}

fn paths_to_end(i: usize, numbers: &[i64], memory: &mut BTreeMap<usize, usize>) -> usize {
    if i == numbers.len() - 1 {
        return 1;
    }
    if let Some(&ans) = memory.get(&i) {
        return ans;
    }
    let ans = (i + 1..numbers.len())
        .take_while(|&j| numbers[j] - numbers[i] <= 3)
        .map(|j| paths_to_end(j, numbers, memory))
        .sum();

    memory.insert(i, ans);
    ans
}
