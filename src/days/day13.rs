use aoc_runner_derive::*;

#[aoc(day13, part1)]
fn part1(input: &str) -> i32 {
    let mut lines = input.lines();
    let time: i32 = lines.next().unwrap().parse().unwrap();
    let busses = lines
        .next()
        .unwrap()
        .split(",")
        .filter_map(|bus| bus.parse().ok());

    let (id, waittime) = busses
        .map(|bus: i32| {
            let mut waittime = time % bus;
            if waittime > 0 {
                waittime = bus - waittime;
            }
            (bus, waittime)
        })
        .min_by_key(|(_, waittime)| *waittime)
        .unwrap();
    id * waittime
}

#[aoc(day13, part2)]
fn part2(input: &str) -> i64 {
    let lines = input.lines();
    let busses = lines
        .skip(1)
        .next()
        .unwrap()
        .split(",")
        .enumerate()
        .filter_map(|(i, b)| {
            let b: i64 = b.parse().ok()?;
            let i = b - (i as i64);
            Some((i, b))
        });

    let (residues, modulii): (Vec<_>, Vec<_>) = busses.unzip();

    for (i, b) in residues.iter().zip(&modulii) {
        println!("x = {} (mod {})", i, b);
    }

    chinese_remainder(&residues, &modulii).unwrap()
}

// rosetta code

fn egcd(a: i64, b: i64) -> (i64, i64, i64) {
    if a == 0 {
        (b, 0, 1)
    } else {
        let (g, x, y) = egcd(b % a, a);
        (g, y - (b / a) * x, x)
    }
}

fn mod_inv(x: i64, n: i64) -> Option<i64> {
    let (g, x, _) = egcd(x, n);
    if g == 1 {
        Some((x % n + n) % n)
    } else {
        None
    }
}

fn chinese_remainder(residues: &[i64], modulii: &[i64]) -> Option<i64> {
    let prod = modulii.iter().product::<i64>();

    let mut sum = 0;

    for (&residue, &modulus) in residues.iter().zip(modulii) {
        let p = prod / modulus;
        sum += residue * mod_inv(p, modulus)? * p
    }

    Some(sum % prod)
}
