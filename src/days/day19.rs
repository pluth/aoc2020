use aoc_runner_derive::*;
use std::collections::{HashMap, HashSet};

fn test(input: &str, rules: &HashMap<i64, Rule>) -> bool {
    let mut booleans = HashSet::new();

    let n = input.len();
    for (s, c) in input.chars().enumerate() {
        for (&i, rule) in rules {
            match rule {
                &Rule::Terminal(r) if c == r => {
                    booleans.insert((1, s + 1, i));
                }
                _ => (),
            };
        }
    }

    for l in 2..=n {
        for s in 1..=(n - l + 1) {
            for p in 1..=(l - 1) {
                for (&a, rule) in rules {
                    match rule {
                        Rule::Terminal(_) | Rule::Terminal2(_, _) => (),
                        Rule::Prod1(first) => {
                            let (b, c) = (first[0], first[1]);
                            if booleans.contains(&(p, s, b))
                                && booleans.contains(&(l - p, s + p, c))
                            {
                                booleans.insert((l, s, a));
                            }
                        }
                        Rule::Prod2(first, second) => {
                            let (b1, c1) = (first[0], first[1]);
                            let (b2, c2) = (second[0], second[1]);
                            if booleans.contains(&(p, s, b1))
                                && booleans.contains(&(l - p, s + p, c1))
                            {
                                booleans.insert((l, s, a));
                            }
                            if booleans.contains(&(p, s, b2))
                                && booleans.contains(&(l - p, s + p, c2))
                            {
                                booleans.insert((l, s, a));
                            }
                        }
                    }
                }
            }
        }
    }

    booleans.contains(&(n, 1, 1))
}

#[derive(Debug, Clone)]
enum Rule {
    Terminal(char),
    Terminal2(char, char),
    Prod1(Vec<i64>),
    Prod2(Vec<i64>, Vec<i64>),
}

impl Rule {
    fn assert_terminal(&self) -> char {
        match self {
            Rule::Terminal(c) => *c,
            _ => panic!("not terminal"),
        }
    }
}

#[aoc(day19, part1)]
fn part1(input: &str) -> usize {
    let mut parts = input.split("\n\n");
    let mut rules: HashMap<i64, Rule> = parts
        .next()
        .unwrap()
        .lines()
        .map(|line| {
            let mut parts = line.split(':');
            let index = parts.next().unwrap().trim().parse().unwrap();

            let rule = parts.next().unwrap().trim();
            let rule = if let Some(rule) = rule.strip_prefix('"') {
                if let Some(rule) = rule.strip_suffix('"') {
                    Rule::Terminal(rule.chars().next().unwrap())
                } else {
                    panic!("malformed")
                }
            } else {
                let mut parts = rule.split('|');
                let first = parts.next().unwrap().trim();
                let first = first
                    .split_whitespace()
                    .map(|s| s.parse().unwrap())
                    .collect();
                if let Some(second) = parts.next() {
                    let second = second
                        .split_whitespace()
                        .map(|s| s.parse().unwrap())
                        .collect();
                    Rule::Prod2(first, second)
                } else {
                    Rule::Prod1(first)
                }
            };

            (index, rule)
        })
        .collect();

    let to_fix: Vec<_> = rules
        .iter()
        .filter(|(_i, rule)| match rule {
            Rule::Prod1(r) if r.len() == 1 => true,
            Rule::Prod2(r, r2) if r.len() == 1 || r2.len() == 1 => true,
            _ => false,
        })
        .map(|(i, rule)| (*i, rule.clone()))
        .collect();

    for (a, rule) in to_fix {
        println!("{:?}", rule);
        match rule {
            Rule::Prod1(r) => {
                println!("{:?}", rules.get(&r[0]));
                rules.insert(a, rules.get(&r[0]).unwrap().clone());
            }
            Rule::Prod2(r, r2) => {
                let first = rules.get(&r[0]).unwrap().assert_terminal();
                let second = rules.get(&r2[0]).unwrap().assert_terminal();
                println!("{:?} , {:?}", first, second);
                rules.insert(a, Rule::Terminal2(first, second));
            }
            _ => (),
        }
    }

    let input = parts.next().unwrap();

    input.lines().filter(|line| test(line, &rules)).count()
}
