use std::collections::HashSet;

use aoc_runner_derive::*;

#[aoc(day6, part1)]
fn part1(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|group| {
            let set: HashSet<char> = group.chars().filter(|c| !c.is_whitespace()).collect();
            set.len()
        })
        .sum()
}

#[aoc(day6, part2)]
fn part2(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|group| {
            let mut sets = group.lines().map(|l| l.chars().collect::<HashSet<_>>());
            let first = sets.next().unwrap();
            let set = sets.fold(first, |ref acc, ref set| acc & set);
            set.len()
        })
        .sum()
}

#[aoc(day6, part1, bits)]
fn part12(input: &str) -> u32 {
    input
        .split("\n\n")
        .map(|group| {
            group
                .chars()
                .filter(|c| !c.is_whitespace())
                .fold(0u32, |acc, v| acc | 1 << (v as u8 - b'a'))
                .count_ones()
        })
        .sum()
}

#[aoc(day6, part2, bits)]
fn part22(input: &str) -> u32 {
    input
        .split("\n\n")
        .map(|group| {
            group
                .split_whitespace()
                .map(|part| part.bytes().fold(0u32, |acc, v| acc | 1 << (v - b'a')))
                .fold(!0, |acc, v| acc & v)
                .count_ones()
        })
        .sum()
}
