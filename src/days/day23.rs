use std::{iter, mem::replace};

use aoc_runner_derive::*;

#[aoc(day23, part1)]
fn part1(input: &str) -> String {
    println!("input: {}", input);
    let mut cups: Vec<_> = input.chars().map(|c| c.to_digit(10).unwrap()).collect();
    let max_label = cups.iter().copied().max().unwrap();

    for i in 0..100 {
        println!("move {}", i + 1);
        println!("cups = {:?}", &cups);
        let mut cups_iter = cups.into_iter();

        let label = cups_iter.next().unwrap();
        let one = cups_iter.next().unwrap();
        let two = cups_iter.next().unwrap();
        let three = cups_iter.next().unwrap();

        let mut destination = label - 1;

        let (index, _dest_label) = loop {
            let find = cups_iter.as_slice().iter();
            if let Some((index, label)) = find.enumerate().find(|&(_i, &l)| l == destination) {
                break (index, label);
            } else {
                if destination == 0 {
                    destination = max_label as u32 + 1;
                }
                destination -= 1;
            }
        };

        println!("current = {}", label);
        println!("picked = {:?}", [one, two, three]);

        println!("destination = {}, index = {}", destination, index);
        // break;

        let (first_part, last_part) = cups_iter.as_slice().split_at(index + 1);

        cups = first_part
            .iter()
            .copied()
            .chain([one, two, three].iter().copied())
            .chain(last_part.iter().copied())
            .chain(iter::once(label))
            .collect();
        println!();
    }

    let (i, _) = cups.iter().enumerate().find(|(_, l)| **l == 1).unwrap();
    cups.rotate_left(i);
    let f: String = cups
        .into_iter()
        .map(|i| std::char::from_digit(i, 10).unwrap())
        .collect();
    f
}

fn successor(i: usize, map: &[usize]) -> usize {
    map[i]
}

#[aoc(day23, part2)]
fn part2(input: &str) -> usize {
    let input: Vec<_> = input
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect();
    let mut current = input.first().copied().unwrap();

    const MAX: usize = 1_000_000;
    let mut map = vec![0; MAX + 1];
    for i in 0..MAX {
        map[i] = i + 1;
    }

    let last = input.last().copied().unwrap();
    for &[a, b] in input.array_windows() {
        map[a] = b;
    }

    map[MAX] = current;
    map[last] = input.len() + 1;

    for _i in 0..10_000_000 {
        // println!("move {}", i+1);

        let one = successor(current, &map);
        let two = successor(one, &map);
        let three = successor(two, &map);

        let mut destination = current - 1;
        if destination == 0 {
            destination = MAX;
        }
        while [one, two, three].contains(&destination) {
            destination -= 1;
            if destination == 0 {
                destination = MAX;
            }
        }

        // println!("current = {}", current);
        // println!("picked = {:?}", [one, two, three]);

        // println!("destination = {}", destination);

        // map.insert(current, successor(three, &map));
        map[current] = successor(three, &map);
        let dest_succ = replace(&mut map[destination], one);
        map[three] = dest_succ;

        current = successor(current, &map);
        // println!();
    }

    let first = successor(1, &map);
    let second = successor(first, &map);

    first as usize * second as usize
}
