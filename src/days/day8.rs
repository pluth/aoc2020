use std::str::FromStr;

use anyhow::bail;
use aoc_runner_derive::*;
use bitvec::{bitvec, prelude::BitVec};

#[derive(Debug, Copy, Clone)]
#[repr(u8)]
enum Op {
    Nop,
    Acc,
    Jmp,
}

#[derive(Debug, Copy, Clone)]
struct Instruction {
    op: Op,
    arg: i32,
}

impl FromStr for Instruction {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut splits = s.split_whitespace();
        let op = splits.next().unwrap();
        let arg = splits.next().unwrap();

        let op = match op {
            "nop" => Op::Nop,
            "acc" => Op::Acc,
            "jmp" => Op::Jmp,
            _ => bail!("bad op code: {}", op),
        };

        let arg = arg.parse()?;

        Ok(Instruction { op, arg })
    }
}

type Program = Vec<Instruction>;

struct State {
    accumulator: i32,
    instruction_pointer: i32,
}

struct Computer {
    state: State,
    program: Program,
}

impl Computer {
    fn execute_next_instruction(&mut self) -> ProgramRet {
        if self.state.instruction_pointer < 0 {
            return ProgramRet::SigSegv;
        }
        if self.state.instruction_pointer as usize == self.program.len() {
            return ProgramRet::Terminated;
        } else if self.state.instruction_pointer as usize > self.program.len() {
            return ProgramRet::SigSegv;
        }

        let Instruction { op, arg } = self.program[self.state.instruction_pointer as usize];
        match op {
            Op::Nop => {
                self.state.instruction_pointer += 1;
            }
            Op::Acc => {
                self.state.accumulator += arg;
                self.state.instruction_pointer += 1;
            }
            Op::Jmp => {
                self.state.instruction_pointer += arg;
            }
        }

        ProgramRet::Running
    }

    fn instruction_pointer(&self) -> i32 {
        self.state.instruction_pointer
    }
}

#[derive(Debug)]
enum ProgramRet {
    Running,
    Terminated,
    SigSegv,
}

#[derive(Debug)]
enum LoopRet {
    Looped,
    Ok(ProgramRet),
}

struct LoopDetector {
    map: BitVec,
    computer: Computer,
}

impl LoopDetector {
    fn step(&mut self) -> LoopRet {
        let next = self.computer.instruction_pointer();
        if let Some(true) = self.map.get(next as usize) {
            return LoopRet::Looped;
        } else if next < self.computer.program.len() as i32 {
            self.map.set(next as usize, true);
        }

        LoopRet::Ok(self.computer.execute_next_instruction())
    }
}

fn make_loop_d(input: &str) -> anyhow::Result<LoopDetector> {
    let program: Program = input
        .lines()
        .map(|l| l.parse())
        .collect::<Result<_, anyhow::Error>>()?;

    let computer = Computer {
        program,
        state: State {
            instruction_pointer: 0,
            accumulator: 0,
        },
    };

    let loop_d = LoopDetector {
        map: bitvec![0; computer.program.len()],
        computer,
    };
    Ok(loop_d)
}

#[aoc(day8, part1)]
fn part1(input: &str) -> anyhow::Result<i32> {
    let mut loop_d = make_loop_d(input)?;

    while let LoopRet::Ok(_) = loop_d.step() {}

    Ok(loop_d.computer.state.accumulator)
}

#[aoc(day8, part2)]
fn part2(input: &str) -> anyhow::Result<i32> {
    let mut loop_d = make_loop_d(input)?;

    println!("testing {} variations", loop_d.computer.program.len());
    for i in 0..loop_d.computer.program.len() {
        if try_variant(i, &mut loop_d) {
            return Ok(loop_d.computer.state.accumulator);
        }
        loop_d.map.set_all(false);
        loop_d.computer.state = State {
            accumulator: 0,
            instruction_pointer: 0,
        };
    }
    panic!("not found")
}

fn try_variant(switch: usize, loop_d: &mut LoopDetector) -> bool {
    if !switch_nop_jmp(switch, &mut loop_d.computer.program) {
        return false;
    }

    let result = loop {
        match loop_d.step() {
            LoopRet::Ok(ProgramRet::Terminated) => break true,
            LoopRet::Ok(ProgramRet::Running) => continue,
            _error => {
                // println!("error {:?}", error);
                break false;
            }
        }
    };

    switch_nop_jmp(switch, &mut loop_d.computer.program);
    result
}

fn switch_nop_jmp(switch: usize, program: &mut Program) -> bool {
    match program[switch] {
        Instruction { op: Op::Jmp, .. } => {
            program[switch].op = Op::Nop;
            true
        }
        Instruction { op: Op::Nop, .. } => {
            program[switch].op = Op::Jmp;
            true
        }
        _ => false,
    }
}
