use aoc_runner_derive::*;

fn binary_space2(code: impl Iterator<Item = bool>) -> i32 {
    let mut result = 0;
    for set in code {
        result <<= 1;
        if set {
            result |= 1;
        }
    }
    result
}

fn binary_space(code: &str) -> (i32, i32) {
    let back = binary_space2(code.chars().filter_map(|c| match c {
        'F' => Some(false),
        'B' => Some(true),
        _ => None,
    }));

    let right = binary_space2(code.chars().filter_map(|c| match c {
        'L' => Some(false),
        'R' => Some(true),
        _ => None,
    }));

    (back, right)
}

#[aoc(day5, part1)]
fn part1(input: &str) -> i32 {
    input
        .lines()
        .map(|line| binary_space(line))
        .map(|(back, right)| back * 8 + right)
        .max()
        .unwrap()
}

#[aoc(day5, part2)]
fn part2(input: &str) -> i32 {
    let mut seats: Vec<_> = input
        .lines()
        .map(|line| binary_space(line))
        .map(|(back, right)| back * 8 + right)
        .collect();
    seats.sort_unstable();

    let mut windows = seats.windows(2);
    while let Some([first, second, ..]) = windows.next() {
        if first + 2 == *second {
            return first + 1;
        }
    }
    panic!("not found");
}

#[test]
fn test() {
    let inputs = &["BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"];
    for input in inputs {
        dbg!(binary_space(input));
    }
}
