// use std::collections::HashMap;

// use aoc_runner_derive::*;
// use itertools::Itertools;

// #[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
// enum Seat {
//     Vacant = 1,
//     Occupied,
// }

// #[derive(Debug, Clone, PartialEq)]
// struct Area {
//     width: usize,
//     height: usize,
//     area: Vec<Option<Seat>>,
// }

// impl Area {
//     fn sees(&self, index: usize, direction: Coords) -> Option<Seat> {
//         let x = index % self.width;
//         let y = index / self.width;
//         let mut see = (y + direction.0, x + direction.1);

//         while see.0 < self.height && see.1 < self.width {
//             if let Some(seat) = self[see] {
//                 return Some(seat)
//             }
//             see = (see.0 + direction.0, see.1 + direction.1);
//         }

//         None
//     }
// }

// impl Index<usize> for Area {
//     type Output = [Option<Seat>];

//     fn index(&self, index: usize) -> &Self::Output {
//         &self.area[index*self.width..(index+1)*self.width]
//     }
// }

// type Coords = (usize, usize);

// impl Index<Coords> for Area {
//     type Output = Option<Seat>;

//     fn index(&self, (y, x): Coords) -> &Self::Output {
//         &self.area[y * self.width + x]
//     }
// }

// #[aoc(day11, part2)]
// fn part2(input: &str) -> usize {
//     let mut area = Area {
//         width: 0,
//         height: 0,
//         area: Vec::new(),
//     };
//     for line in input.lines() {
//         area.width = line.len();
//         area.area.extend(line.chars().map(|c| match c {
//             'L' => Some(Seat::Vacant),
//             '.' => None,
//             '#' => Some(Seat::Occupied),
//             _ => panic!("bad field: {}", c),
//         }));
//         area.height += 1;
//     }

//     // loop {
//     //     let mut new_area = area.clone();
//     //     for seat in area.area.iter().filter_map(|s| s.as_ref()) {
//     //         const DIRECTIONS: &[Coords] = &[(-1,-1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1,0), (1, 1)];
//     //         let neighbours = DIRECTIONS.map(|d| area.sees(, direction))
//     //     }
//     // }

//     todo!()
// type Coords = (usize, usize);
// type Area = HashMap<Coords, Seat>;

// #[derive(Debug, Clone, Copy, Eq, PartialEq)]
// enum Seat {
//     Occupied,
//     Empty,
// }

// #[aoc_generator(day11)]
// fn generate(input: &str) -> Area {
//     let mut area = HashMap::new();
//     for (y, line) in input.lines().enumerate() {
//         for (x, c) in line.chars().enumerate() {
//             match c {
//                 'L' => area.insert((x, y), Seat::Empty),
//                 '#' => area.insert((x, y), Seat::Occupied),
//                 _ => None,
//             };
//         }
//     }
//     area
// }

// fn neighbours((x, y): Coords) -> impl Iterator<Item = Coords> {
//     (x.saturating_sub(1)..=x + 1)
//         .cartesian_product(y.saturating_sub(1)..=y + 1)
//         .filter(move |coords| *coords != (x, y))
// }

// fn occupied_neighbours(coords: Coords, area: &Area) -> usize {
//     neighbours(coords)
//         .filter_map(|ref coords| area.get(coords))
//         .filter(|seat| matches!(seat, Seat::Occupied))
//         .count()
// }

// fn simulate1(area: &Area, new_area: &mut Area) {
//     for (&coords, seat) in area {
//         let occupied = occupied_neighbours(coords, area);
//         match seat {
//             Seat::Occupied => {
//                 if occupied >= 4 {
//                     new_area.insert(coords, Seat::Empty)
//                 } else {
//                     new_area.insert(coords, Seat::Occupied)
//                 };
//             }
//             Seat::Empty => {
//                 if occupied == 0 {
//                     new_area.insert(coords, Seat::Occupied)
//                 } else {
//                     new_area.insert(coords, Seat::Empty)
//                 };
//             }
//         }
//     }
// }

// #[aoc(day11, part1)]
// fn part1(input: &Area) -> usize {
//     let [mut first, mut second] = [input.clone(), Area::with_capacity(input.len())];
//     let mut i = 0;
//     let f = loop {
//         if i % 2 == 0 {
//             simulate1(&first, &mut second);
//         } else {
//             simulate1(&second, &mut first);
//         }

//         if first == second {
//             break &first
//         }

//         i += 1;
//     };

//     f.values().filter(|s| matches!(s, Seat::Occupied)).count()
// }

// fn visible_neighbours((x,y): Coords, area: Area) {
//     const DIRECTIONS: [()]
// }

// #[aoc(day11, part2)]
// fn part2(input: &Area) -> usize {

// }
