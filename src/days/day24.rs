use std::collections::{HashMap, HashSet};

use aoc_runner_derive::*;

enum Direction {
    East,
    West,
    SouthEast,
    NorthWest,
    NorthEast,
    SouthWest,
}

#[aoc(day24, part1)]
fn part1(input: &str) -> usize {
    parse(input).len()
}

fn parse(input: &str) -> HashSet<(i64, i64)> {
    let mut black_tiles = HashSet::new();

    for line in input.lines() {
        let mut chars = line.chars();
        let directions = std::iter::from_fn(move || {
            let next = chars.next()?;
            match next {
                'n' => match chars.next()? {
                    'w' => Some(Direction::NorthWest),
                    'e' => Some(Direction::NorthEast),
                    d => panic!("unexpected: {}{}", next, d),
                },
                's' => match chars.next()? {
                    'w' => Some(Direction::SouthWest),
                    'e' => Some(Direction::SouthEast),
                    d => panic!("unexpected: {}{}", next, d),
                },
                'e' => Some(Direction::East),
                'w' => Some(Direction::West),
                _ => panic!("unexpected: {}", next),
            }
        });

        let mut west_east = 0;
        let mut southeast_northwest = 0;
        let mut northeast_southwest = 0;

        for d in directions {
            match d {
                Direction::East => west_east += 1,
                Direction::West => west_east -= 1,
                Direction::SouthEast => southeast_northwest -= 1,
                Direction::NorthWest => southeast_northwest += 1,
                Direction::NorthEast => northeast_southwest -= 1,
                Direction::SouthWest => northeast_southwest += 1,
            }
        }

        // println!("{:?}", (west_east, southeast_northwest, northeast_southwest));
        let normalized = normalize(west_east, southeast_northwest, northeast_southwest);
        // println!("{:?}", normalize(west_east, southeast_northwest, northeast_southwest));
        // println!();
        if black_tiles.contains(&normalized) {
            black_tiles.remove(&normalized);
        } else {
            black_tiles.insert(normalized);
        }
    }

    // println!("{:?}", black_tiles);
    black_tiles
}

fn normalize(west_east: i64, southeast_northwest: i64, northeast_southwest: i64) -> (i64, i64) {
    (
        southeast_northwest - west_east,
        northeast_southwest - west_east,
    )
}

#[aoc(day24, part2)]
fn part2(input: &str) -> usize {
    let mut black_tiles = parse(input);

    for _i in 0..100 {
        const NEIGHBOURS: &[(i64, i64)] = &[(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, -1)];

        let mut neighbours = HashMap::with_capacity(black_tiles.len());
        for (x, y) in &black_tiles {
            for (dx, dy) in NEIGHBOURS {
                neighbours
                    .entry((x + dx, y + dy))
                    .and_modify(|e| *e += 1)
                    .or_insert(1);
            }
        }

        let mut new_black = HashSet::with_capacity(black_tiles.len());

        for (&(x, y), &n) in &neighbours {
            if !black_tiles.contains(&(x, y)) {
                // white
                if n == 2 {
                    new_black.insert((x, y));
                }
            } else {
                // black
                if n == 1 || n == 2 {
                    new_black.insert((x, y));
                }
            }
        }

        black_tiles = new_black;
        println!("day {}: {}", _i + 1, black_tiles.len());
    }

    black_tiles.len()
}
