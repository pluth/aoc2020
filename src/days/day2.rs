use anyhow::anyhow;
use aoc_runner_derive::*;

#[derive(Debug)]
struct Policy {
    min: usize,
    max: usize,
    letter: char,
}

#[derive(Debug)]
struct TestCase<'a> {
    password: &'a str,
    policy: Policy,
}

fn parse_input<'a>(input: &'a str) -> anyhow::Result<Vec<TestCase<'a>>> {
    let re = regex::Regex::new(r"^(\d+)\-(\d+) (\w): (.+)")?;
    input
        .lines()
        .map(|l| {
            let capture = re.captures(l).ok_or(anyhow!("capture failed"))?;
            let policy = Policy {
                min: capture.get(1).unwrap().as_str().parse()?,
                max: capture.get(2).unwrap().as_str().parse()?,
                letter: capture.get(3).unwrap().as_str().parse()?,
            };
            let case = TestCase {
                password: capture.get(4).unwrap().as_str(),
                policy,
            };
            Ok(case)
        })
        .collect()
}

fn test_policy(case: &TestCase<'_>) -> bool {
    let range = case.policy.min..=case.policy.max;
    range.contains(
        &case
            .password
            .chars()
            .filter(|c| *c == case.policy.letter)
            .count(),
    )
}

fn test_policy2(TestCase { password, policy }: &TestCase<'_>) -> bool {
    let check_first = password
        .chars()
        .nth(policy.min - 1)
        .map(|c| c == policy.letter)
        .unwrap_or(false);
    let check_second = password
        .chars()
        .nth(policy.max - 1)
        .map(|c| c == policy.letter)
        .unwrap_or(false);
    let check = (check_first && !check_second) || (!check_first && check_second);
    check
}

#[aoc(day2, part1)]
fn part1<'a>(input: &str) -> anyhow::Result<usize> {
    let input = parse_input(input)?;
    let count = input.iter().filter(|c| test_policy(c)).count();
    Ok(count)
}

#[aoc(day2, part2)]
fn part2(input: &str) -> anyhow::Result<usize> {
    let input = parse_input(input)?;
    let count = input.iter().filter(|c| test_policy2(c)).count();
    Ok(count)
}

#[test]
fn test() {
    let data = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
    let r = part2(data).unwrap();
    assert_eq!(r, 1)
}
