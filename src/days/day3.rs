use aoc_runner_derive::*;

#[aoc(day3, part1)]
fn part1(input: &str) -> usize {
    let mut right = 0;
    let mut counter = 0;
    for line in input.lines().skip(1) {
        let len = line.len();
        right += 3;
        right %= len;
        if line.as_bytes()[right] == '#' as u8 {
            counter += 1;
        }
    }

    counter
}

#[aoc(day3, part2)]
fn part2(input: &str) -> usize {
    let spec = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    spec.iter()
        .map(|&(right, down)| {
            input
                .lines()
                .step_by(down)
                .fold((0, 0), |(pos, mut counter), line| {
                    if line.as_bytes()[pos] == '#' as u8 {
                        counter += 1;
                    }
                    ((pos + right) % line.len(), counter)
                })
                .1
        })
        .product()
}
