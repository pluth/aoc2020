use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum Token {
    Integer(i64),
    LBracket,
    RBracket,
    OperatorAdd,
    OperatorMul,
}

fn lex_line(input: &str) -> impl Iterator<Item = Token> + '_ {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"\-?[0-9]+|\(|\)|\+|\*").unwrap();
    }

    RE.find_iter(input).map(|m| match m.as_str() {
        "(" => Token::LBracket,
        ")" => Token::RBracket,
        "+" => Token::OperatorAdd,
        "*" => Token::OperatorMul,
        number => Token::Integer(number.parse().unwrap()),
    })
}

mod part1 {
    use std::iter;

    use aoc_runner_derive::*;
    use iter::Peekable;
    use tracing_subscriber::EnvFilter;

    use super::*;

    #[tracing::instrument(skip(tokens))]
    fn operand(tokens: &mut Peekable<impl Iterator<Item = Token>>) -> i64 {
        if tokens.next_if_eq(&Token::LBracket).is_some() {
            let first = expression(tokens);
            assert!(
                matches!(tokens.next(), Some(Token::RBracket)),
                "expected rbracket"
            );
            first
        } else if let Some(Token::Integer(i)) = tokens.next_if(|t| matches!(t, Token::Integer(_))) {
            tracing::info!("integer {}", i);
            i
        } else {
            panic!("expected int to expr");
        }
    }

    #[tracing::instrument(skip(tokens))]
    fn expression(tokens: &mut Peekable<impl Iterator<Item = Token>>) -> i64 {
        tracing::info!("expression");
        let mut acc = operand(tokens);

        loop {
            if tokens.next_if_eq(&Token::OperatorAdd).is_some() {
                tracing::info!("+");
                acc += operand(tokens);
            } else if tokens.next_if_eq(&Token::OperatorMul).is_some() {
                tracing::info!("*");
                acc *= operand(tokens);
            } else {
                tracing::info!("not operator, breaking: {:?}", tokens.peek());
                break;
            }
        }

        acc
    }

    #[aoc(day18, part1)]
    fn part1(input: &str) -> i64 {
        let _ = tracing_subscriber::fmt::fmt()
            .with_env_filter(EnvFilter::from_default_env())
            .try_init();

        input
            .lines()
            .map(|line| {
                tracing::warn!("{}", line);
                let mut tokens = lex_line(line).peekable();
                expression(&mut tokens)
            })
            .sum()
    }
}

mod part2 {
    use std::iter;

    use aoc_runner_derive::*;
    use iter::Peekable;
    use tracing_subscriber::EnvFilter;

    use super::*;

    #[tracing::instrument(skip(tokens))]
    fn sum(tokens: &mut Peekable<impl Iterator<Item = Token>>) -> i64 {
        let mut acc = operand(tokens);

        loop {
            if tokens.next_if_eq(&Token::OperatorAdd).is_some() {
                tracing::info!("+");
                acc += operand(tokens);
            } else {
                tracing::info!("not +, breaking: {:?}", tokens.peek());
                break;
            }
        }
        acc
    }

    #[tracing::instrument(skip(tokens))]
    fn expression(tokens: &mut Peekable<impl Iterator<Item = Token>>) -> i64 {
        tracing::info!("expression");
        let mut acc = sum(tokens);

        loop {
            if tokens.next_if_eq(&Token::OperatorMul).is_some() {
                tracing::info!("*");
                acc *= sum(tokens);
            } else {
                tracing::info!("not operator, breaking: {:?}", tokens.peek());
                break;
            }
        }

        acc
    }

    #[tracing::instrument(skip(tokens))]
    fn operand(tokens: &mut Peekable<impl Iterator<Item = Token>>) -> i64 {
        if tokens.next_if_eq(&Token::LBracket).is_some() {
            let first = expression(tokens);
            assert!(
                matches!(tokens.next(), Some(Token::RBracket)),
                "expected rbracket"
            );
            first
        } else if let Some(Token::Integer(i)) = tokens.next_if(|t| matches!(t, Token::Integer(_))) {
            tracing::info!("integer {}", i);
            i
        } else {
            panic!("expected int to expr");
        }
    }

    #[aoc(day18, part2)]
    fn part2(input: &str) -> i64 {
        let _ = tracing_subscriber::fmt::fmt()
            .with_env_filter(EnvFilter::from_default_env())
            .try_init();

        input
            .lines()
            .map(|line| {
                tracing::warn!("{}", line);
                let mut tokens = lex_line(line).peekable();
                expression(&mut tokens)
            })
            .inspect(|sum| println!("{}", sum))
            .sum()
        // .next().unwrap_or_default()
    }
}
