use std::collections::BTreeSet;

type Collection<T> = BTreeSet<T>;

use aoc_runner_derive::*;

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> anyhow::Result<Collection<u32>> {
    let set = input.lines().map(|i| i.parse()).collect::<Result<_, _>>()?;
    Ok(set)
}

#[aoc(day1, part1)]
fn part1(input: &Collection<u32>) -> u32 {
    find_pair(2020, input).unwrap()
}

#[aoc(day1, part2)]
fn part2(input: &Collection<u32>) -> u32 {
    for first in input {
        let remainder = 2020 - first;
        if let Some(found) = find_pair(remainder, input) {
            return first * found;
        }
    }
    panic!("not found");
}

fn find_pair(target: u32, input: &Collection<u32>) -> Option<u32> {
    for member in input {
        let other = target - member;
        if input.contains(&other) {
            return Some(member * other);
        }
    }
    None
}
