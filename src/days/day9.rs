use core::panic;
use std::str::FromStr;

use aoc_runner_derive::*;
use itertools::Itertools;

#[aoc(day9, part1)]
fn part1(input: &str) -> anyhow::Result<i64> {
    let numbers: Vec<i64> = input
        .lines()
        .map(|l| l.parse())
        .collect::<Result<_, <i64 as FromStr>::Err>>()?;

    for window in numbers.windows(26) {
        let target = window[25];
        if !window[..25]
            .iter()
            .tuple_combinations()
            .any(|(a, b)| a + b == target)
        {
            return Ok(target);
        }
    }
    panic!("not found")
}

#[aoc(day9, part2)]
fn part2(input: &str) -> anyhow::Result<i64> {
    let numbers: Vec<i64> = input
        .lines()
        .map(|l| l.parse())
        .collect::<Result<_, <i64 as FromStr>::Err>>()?;

    let target = 23278925;

    for i in 2..numbers.len() {
        for window in numbers.windows(i) {
            if window.iter().sum::<i64>() == target {
                let (min, max) = window.iter().minmax().into_option().unwrap();
                return Ok(min + max);
            }
        }
    }
    panic!("not found")
}

#[aoc(day9, part2, caterpillar)]
fn part22(input: &str) -> anyhow::Result<i64> {
    let numbers: Vec<i64> = input
        .lines()
        .map(|l| l.parse())
        .collect::<Result<_, <i64 as FromStr>::Err>>()?;
    let target = 23278925;

    let mut begin = 0;
    let mut end = 0;
    let mut sum = 0;

    while begin != numbers.len() {
        if sum == target {
            let (min, max) = numbers[begin..end].iter().minmax().into_option().unwrap();
            return Ok(min + max);
        } else if sum < target {
            sum += numbers[end];
            end += 1;
        } else {
            sum -= numbers[begin];
            begin += 1;
        }
    }

    todo!()
}
