use aoc_runner_derive::*;

#[aoc(day12, part1)]
fn part1(input: &str) -> i32 {
    let (mut east, mut north) = (0i32, 0i32);
    let mut dir = (1, 0);

    for line in input.lines() {
        let (op, arg) = line.split_at(1);
        let arg: i32 = arg.parse().unwrap();
        match op {
            "N" => {
                north += arg;
            }
            "S" => {
                north -= arg;
            }
            "E" => {
                east += arg;
            }
            "W" => {
                east -= arg;
            }
            "F" => {
                east += arg * dir.0;
                north += arg * dir.1;
            }
            "L" => {
                dir = rotate(dir, arg);
            }
            "R" => {
                dir = rotate(dir, -arg);
            }
            _ => (),
        }
    }
    east.abs() + north.abs()
}

fn rotate((x, y): (i32, i32), angle: i32) -> (i32, i32) {
    match angle {
        180 | -180 => (-x, -y),
        90 | -270 => match (x, y) {
            (1, 0) => (0, 1),
            (0, 1) => (-1, 0),
            (-1, 0) => (0, -1),
            (0, -1) => (1, 0),
            _ => panic!("bad dir"),
        },
        -90 | 270 => match (x, y) {
            (1, 0) => (0, -1),
            (0, 1) => (1, 0),
            (-1, 0) => (0, 1),
            (0, -1) => (-1, 0),
            _ => panic!("bad dir"),
        },
        _ => panic!("bad angle"),
    }
}

#[aoc(day12, part2)]
fn part2(input: &str) -> i32 {
    let mut waypoint = (10i32, 1i32);
    let (mut east, mut north) = (0, 0);

    for line in input.lines() {
        let (op, arg) = line.split_at(1);
        let arg: i32 = arg.parse().unwrap();
        match op {
            "N" => {
                waypoint.1 += arg;
            }
            "S" => {
                waypoint.1 -= arg;
            }
            "E" => {
                waypoint.0 += arg;
            }
            "W" => {
                waypoint.0 -= arg;
            }
            "F" => {
                east += arg * waypoint.0;
                north += arg * waypoint.1;
            }
            "L" => {
                waypoint = rotate2(waypoint, arg as f32);
            }
            "R" => {
                waypoint = rotate2(waypoint, -arg as f32);
            }
            _ => (),
        }
    }
    east.abs() + north.abs()
}

fn rotate2(vector: (i32, i32), angle: f32) -> (i32, i32) {
    let (sin, cos) = angle.to_radians().sin_cos();
    let x = cos * vector.0 as f32 - sin * vector.1 as f32;
    let y = sin * vector.0 as f32 + cos * vector.1 as f32;
    (x.round() as i32, y.round() as i32)
}
