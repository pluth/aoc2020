use aoc_runner_derive::*;
use itertools::iproduct;
use rayon::prelude::*;
use std::collections::HashSet;

#[aoc(day17, part1)]
fn part1(input: &str) -> usize {
    let mut active = HashSet::new();

    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            print!("({}, {}) ", x, y);
            if c == '#' {
                active.insert((x as i64, y as i64, 0));
            }
        }
        println!();
    }

    // println!("{:?}", active);
    // println!();

    for _ in 0..6 {
        let mut new_active = HashSet::new();
        for x in -15..15 {
            for y in -15..15 {
                for z in -15..15 {
                    let neighbours = active_neighbours((x, y, z), &active);
                    if active.contains(&(x, y, z)) {
                        // println!("{:?} neighbours: {}", (x, y, z), neighbours);
                        if neighbours == 2 || neighbours == 3 {
                            new_active.insert((x, y, z));
                        }
                    } else {
                        if neighbours == 3 {
                            new_active.insert((x, y, z));
                        }
                    }
                }
            }
        }
        active = new_active;
    }

    active.len()
}

fn active_neighbours((x, y, z): (i64, i64, i64), active: &HashSet<(i64, i64, i64)>) -> usize {
    let mut neighbours = 0;
    for dx in -1..=1 {
        for dy in -1..=1 {
            for dz in -1..=1 {
                if (dx, dy, dz) != (0, 0, 0) {
                    if active.contains(&(x + dx, y + dy, z + dz)) {
                        neighbours += 1;
                    }
                }
            }
        }
    }
    neighbours
}

#[aoc(day17, part2)]
fn part2(input: &str) -> usize {
    let mut active: HashSet<_> = input
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter(|&(_, c)| c == '#')
                .map(move |(x, _c)| (x as i64, y as i64, 0, 0))
        })
        .collect();

    let xmax = active.iter().map(|e| e.0).max().unwrap();
    let ymax = active.iter().map(|e| e.1).max().unwrap();
    let rounds = 6;

    for extent in 1..=rounds {
        active = iproduct!(
            -extent..xmax + extent + 1,
            -extent..ymax + extent + 1,
            -extent..extent + 1,
            -extent..extent + 1
        )
        .par_bridge()
        .filter_map(|coords| {
            let neighbours = active_neighbours4(coords, &active);
            let active = active.contains(&coords);
            match (active, neighbours) {
                (true, 2) | (true, 3) => Some(coords),
                (false, 3) => Some(coords),
                _ => None,
            }
        })
        .collect();
    }

    active.len()
}

fn active_neighbours4(
    (x, y, z, w): (i64, i64, i64, i64),
    active: &HashSet<(i64, i64, i64, i64)>,
) -> usize {
    iproduct!(-1..2, -1..2, -1..2, -1..2)
        .filter(|coords| !matches!(coords, (0, 0, 0, 0)))
        .filter(|(dx, dy, dz, dw)| active.contains(&(dx + x, dy + y, dz + z, dw + w)))
        .count()
}
