use aoc_runner_derive::*;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{BTreeMap, HashSet, VecDeque};

#[derive(Debug)]
struct Rule<'a> {
    count: usize,
    color: &'a str,
}

fn parse(input: &str) -> BTreeMap<&'_ str, Vec<Rule<'_>>> {
    lazy_static! {
        static ref RULE_START: Regex = Regex::new("(.*) bags contain ").unwrap();
        static ref RULE_IMPLIES: Regex = Regex::new(r"(\d+) (.*?) bags?[,.]").unwrap();
    }

    let mut rules = BTreeMap::new();
    for line in input.lines() {
        let start = RULE_START.captures(line).unwrap();

        let rule = start.get(1).unwrap();

        let implies: Vec<Rule> = RULE_IMPLIES
            .captures_iter(line)
            .map(|rule| {
                let number = rule.get(1).unwrap();
                let color = rule.get(2).unwrap();

                Rule {
                    count: number.as_str().parse().unwrap(),
                    color: color.as_str().trim(),
                }
            })
            .collect();
        rules.insert(rule.as_str().trim(), implies);
    }

    rules
}

fn find(target: &str, start: &str, map: &BTreeMap<&str, Vec<Rule<'_>>>) -> bool {
    let mut visited = HashSet::new();
    let mut queue = VecDeque::new();
    queue.push_back(start);

    while let Some(next) = queue.pop_front() {
        if next == target {
            return true;
        }

        if let Some(entry) = map.get(&next) {
            for n in entry.iter() {
                if !visited.contains(n.color) {
                    visited.insert(n.color);
                    queue.push_front(n.color);
                }
            }
        } else {
            continue;
        }
    }
    false
}

fn find_bag<'a>(
    map: &BTreeMap<&'a str, Vec<Rule<'a>>>,
    memory: &mut BTreeMap<&'a str, bool>,
    target: &'a str,
    current: &'a str,
) -> bool {
    if current == target {
        return true;
    }
    if let Some(memorized) = memory.get(current) {
        return *memorized;
    }
    let result = map
        .get(current)
        .into_iter()
        .flatten()
        .any(|rule| find_bag(map, memory, target, rule.color));
    memory.insert(current, result);
    result
}

#[aoc(day7, part1, recursive)]
fn part12(input: &str) -> usize {
    let input = parse(input);
    let mut memory = BTreeMap::new();
    input
        .keys()
        .filter(|key| find_bag(&input, &mut memory, "shiny gold", key))
        .count()
        - 1
}

#[aoc(day7, part1)]
fn part1(input: &str) -> usize {
    let input = parse(input);
    input
        .iter()
        .filter(|r| find("shiny gold", r.0, &input))
        .count()
        - 1
}

fn bags(target: &str, map: &BTreeMap<&str, Vec<Rule<'_>>>) -> usize {
    if let Some(entry) = map.get(target) {
        let sum: usize = entry.iter().map(|r| r.count * bags(r.color, map)).sum();
        1_usize + sum
    } else {
        1
    }
}

#[aoc(day7, part2)]
fn part2(input: &str) -> usize {
    let map = parse(input);
    bags("shiny gold", &map) - 1
}

#[cfg(test)]
mod test {
    use super::{part1, part2};

    #[test]
    fn test() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
        dark orange bags contain 3 bright white bags, 4 muted yellow bags.
        bright white bags contain 1 shiny gold bag.
        muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
        shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
        dark olive bags contain 3 faded blue bags, 4 dotted black bags.
        vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
        faded blue bags contain no other bags.
        dotted black bags contain no other bags.";

        let n = part1(input);

        assert_eq!(n, 4);

        assert_eq!(part2(input), 32);
    }

    fn test2() {
        let input = "shiny gold bags contain 2 dark red bags.
        dark red bags contain 2 dark orange bags.
        dark orange bags contain 2 dark yellow bags.
        dark yellow bags contain 2 dark green bags.
        dark green bags contain 2 dark blue bags.
        dark blue bags contain 2 dark violet bags.
        dark violet bags contain no other bags.";

        let n = part2(input);

        assert_eq!(n, 126)
    }
}
