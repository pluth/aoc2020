use std::{
    fmt::{Debug, Display},
    num::ParseIntError,
};

use aoc_runner_derive::aoc;
use serde::{
    de::{IntoDeserializer, MapAccess, Visitor},
    Deserialize, Deserializer,
};

#[derive(Debug, Deserialize)]
struct Passport<'a> {
    byr: i32,
    iyr: i32,
    eyr: i32,
    hgt: &'a str,
    hcl: &'a str,
    ecl: &'a str,
    pid: &'a str,
    cid: Option<&'a str>,
}

impl<'a> Passport<'a> {
    fn validate(&self) -> bool {
        (1920..=2002).contains(&self.byr)
            && (2010..=2020).contains(&self.iyr)
            && (2020..=2030).contains(&self.eyr)
            && self.validate_hgt()
            && self.validate_hcl()
            && self.validate_ecl()
            && self.validate_pid()
    }

    fn validate_hgt(&self) -> bool {
        if let Some(s) = self.hgt.strip_suffix("cm") {
            if let Ok(int) = s.parse() {
                (150..=193).contains(&int)
            } else {
                false
            }
        } else if let Some(s) = self.hgt.strip_suffix("in") {
            if let Ok(int) = s.parse() {
                (59..=76).contains(&int)
            } else {
                false
            }
        } else {
            false
        }
    }

    fn validate_hcl(&self) -> bool {
        if !self.hcl.len() == 7 {
            return false;
        }

        if let Some(hcl) = self.hcl.strip_prefix("#") {
            hcl.chars()
                .all(|c| ('a'..='f').contains(&c) || ('0'..='9').contains(&c))
        } else {
            false
        }
    }

    fn validate_ecl(&self) -> bool {
        let eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
        eye_colors.iter().any(|color| *color == self.ecl)
    }

    fn validate_pid(&self) -> bool {
        self.pid.len() == 9 && self.pid.chars().all(|c| c.is_numeric())
    }
}

use thiserror::Error;
#[derive(Debug, Error)]
enum KVError {
    #[error("{0}")]
    Display(String),
    #[error("{0}")]
    Custom(#[from] anyhow::Error),
    #[error("{0}")]
    Parse(#[from] ParseIntError),
    #[error("no key")]
    NoKey,
    #[error("no value")]
    NoValue,
}
impl serde::de::Error for KVError {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        KVError::Display(msg.to_string())
    }
}

struct ValueDeserializer<'de>(&'de str);

impl<'de> Deserializer<'de> for ValueDeserializer<'de> {
    type Error = KVError;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: Visitor<'de>,
    {
        visitor.visit_borrowed_str(self.0)
    }

    fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: Visitor<'de>,
    {
        let int = self.0.parse()?;
        visitor.visit_i32(int)
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: Visitor<'de>,
    {
        visitor.visit_some(ValueDeserializer(self.0))
    }

    serde::forward_to_deserialize_any! {
        bool i8 i16 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf unit unit_struct newtype_struct seq tuple
        tuple_struct map enum struct identifier ignored_any
    }
}
struct KVDeserializer<'a> {
    pairs: Vec<(&'a str, &'a str)>,
    buffer: Option<&'a str>,
}

impl<'a> KVDeserializer<'a> {
    fn from_line(line: &'a str) -> Result<Self, KVError> {
        let pairs = line
            .split_whitespace()
            .map(|line| {
                let mut split = line.split(":");
                let key = split.next().ok_or(KVError::NoKey)?;
                let value = split.next().ok_or(KVError::NoValue)?;
                Ok((key, value))
            })
            .collect::<Result<_, KVError>>()?;
        Ok(Self {
            pairs,
            buffer: None,
        })
    }
}

impl<'de> MapAccess<'de> for KVDeserializer<'de> {
    type Error = KVError;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
    where
        K: serde::de::DeserializeSeed<'de>,
    {
        if let Some((key, value)) = self.pairs.pop() {
            self.buffer = Some(value);
            seed.deserialize(key.into_deserializer()).map(Some)
        } else {
            Ok(None)
        }
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
    where
        V: serde::de::DeserializeSeed<'de>,
    {
        let s = self.buffer.take().unwrap();
        let value = seed.deserialize(ValueDeserializer(s))?;
        Ok(value)
    }
}

impl<'de> serde::de::Deserializer<'de> for KVDeserializer<'de> {
    type Error = KVError;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
    where
        V: Visitor<'de>,
    {
        visitor.visit_map(self)
    }

    serde::forward_to_deserialize_any! {
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf option unit unit_struct newtype_struct seq tuple
        tuple_struct map enum struct identifier ignored_any
    }
}

fn parse(line: &str) -> anyhow::Result<Passport> {
    let de = KVDeserializer::from_line(line)?;
    let pp = Passport::deserialize(de)?;
    Ok(pp)
}

#[aoc(day4, part1)]
fn part1(input: &str) -> usize {
    input
        .split("\n\n")
        .inspect(|line| println!("{}", line))
        .filter_map(|l| match parse(l) {
            Err(e) => {
                println!("{}", e);
                None
            }
            Ok(pp) => Some(pp),
        })
        .inspect(|pp| println!("{:?}", pp))
        .count()
}

#[aoc(day4, part2)]
fn part2(input: &str) -> usize {
    input
        .split("\n\n")
        .inspect(|line| println!("{}", line))
        .filter_map(|l| match parse(l) {
            Err(e) => {
                println!("{}", e);
                None
            }
            Ok(pp) => Some(pp),
        })
        .inspect(|pp| println!("{:?}", pp))
        .filter(|pp| pp.validate())
        .count()
}
