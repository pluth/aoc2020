use aoc_runner_derive::*;

#[aoc(day15, part1)]
fn part1(input: &str) -> usize {
    make(input, 2020)
}

#[aoc(day15, part2)]
fn part2(input: &str) -> usize {
    make(input, 30_000_000)
}

fn make(input: &str, iterations: usize) -> usize {
    let mut memory: std::collections::HashMap<_, _> = input
        .split(',')
        .map(str::parse)
        .enumerate()
        .map(|(i, n)| n.map(|n| (n, i + 1)))
        .collect::<Result<_, _>>()
        .unwrap();

    (memory.len() + 1..iterations)
        .fold(0, |acc, i| memory.insert(acc, i).map_or(0, |prev| i - prev))
}
