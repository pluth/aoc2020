use aoc_runner_derive::*;
use std::{
    collections::{hash_map::DefaultHasher, HashSet, VecDeque},
    hash::{Hash, Hasher},
};

#[aoc(day22, part1)]
fn part1(input: &str) -> usize {
    let mut input = input.split("\n\n");
    let mut player1: VecDeque<usize> = input
        .next()
        .unwrap()
        .lines()
        .skip(1)
        .map(|l| l.parse().unwrap())
        .collect();
    let mut player2: VecDeque<usize> = input
        .next()
        .unwrap()
        .lines()
        .skip(1)
        .map(|l| l.parse().unwrap())
        .collect();

    for _i in 0.. {
        if player1.is_empty() || player2.is_empty() {
            break;
        }

        let p1 = player1.pop_front().unwrap();
        let p2 = player2.pop_front().unwrap();

        if p1 > p2 {
            player1.push_back(p1);
            player1.push_back(p2);
        } else {
            player2.push_back(p2);
            player2.push_back(p1);
        }

        println!();
    }

    let sum1: usize = player1
        .into_iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (i + 1) as usize * c)
        .sum();
    let sum2: usize = player2
        .into_iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (i + 1) as usize * c)
        .sum();
    sum1 + sum2
}

fn combat(mut player1: Vec<usize>, mut player2: Vec<usize>) -> (usize, Vec<usize>) {
    // https://www.reddit.com/r/adventofcode/comments/khyjgv/2020_day_22_solutions/ggpcsnd
    let p1_max = player1.iter().copied().max();
    if p1_max > player2.iter().copied().max() && p1_max.unwrap() > player1.len() + player2.len() {
        return (1, player1);
    }

    let mut previous_states = HashSet::with_capacity(player1.len().min(player2.len()));

    loop {
        if player1.is_empty() {
            break (2, player2);
        } else if player2.is_empty() {
            break (1, player1);
        }

        let mut hasher = DefaultHasher::new();
        (&player1, &player2).hash(&mut hasher);
        let state = hasher.finish();

        if previous_states.contains(&state) {
            break (1, player1);
        } else {
            previous_states.insert(state);
        }

        let p1 = player1.remove(0);
        let p2 = player2.remove(0);

        if player1.len() >= p1 && player2.len() >= p2 {
            let player1_copy = player1[..p1].to_owned();
            let player2_copy = player2[..p2].to_owned();

            let (winner, _deck) = combat(player1_copy, player2_copy);
            if winner == 1 {
                player1.push(p1);
                player1.push(p2);
            } else {
                player2.push(p2);
                player2.push(p1);
            }
        } else if p1 > p2 {
            player1.push(p1);
            player1.push(p2);
        } else {
            player2.push(p2);
            player2.push(p1);
        }
    }
}

#[aoc(day22, part2)]
fn part2(input: &str) -> usize {
    let mut input = input.split("\n\n");
    let player1: Vec<usize> = input
        .next()
        .unwrap()
        .lines()
        .skip(1)
        .map(|l| l.parse().unwrap())
        .collect();
    let player2: Vec<usize> = input
        .next()
        .unwrap()
        .lines()
        .skip(1)
        .map(|l| l.parse().unwrap())
        .collect();

    let (_winner, deck) = combat(player1, player2);

    deck.into_iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (i + 1) as usize * c)
        .sum()
}
